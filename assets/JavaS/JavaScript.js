
console.log("hello from script");

function canvasPage()
{
    var canvas = document.getElementById("the_canvas");
    var context = canvas.getContext("2d");


    context.fillStyle="blue";
    context.fillRect(20, 9, 50,50);
    context.fillRect(20, 430, 50,50);
    context.fillRect(1030, 430, 50,50);
    context.fillRect(1030, 9, 50,50);

    //big circle
    context.beginPath();
    context.arc(300,250,80,50,Math.PI,true);
    context.stroke();

    //arc
    //290, 250
    context.beginPath();
    context.arc(300,280,40,0,Math.PI,false);
    context.stroke();

    //little circles
    context.beginPath();
    context.arc(340,230,10,50,Math.PI,true);
    context.stroke();
    context.beginPath();
    context.arc(260,230,10,50,Math.PI,true);
    context.stroke();


    //triangle
    context.beginPath();
    context.moveTo(300, 270); //center of the circle
    context.lineTo(300, 230);
    context.fill();
    context.stroke();


    context.beginPath();
    context.fillStyle="yellow";
    context.moveTo(350, 189); //center of the circle
    context.lineTo(300, 120);
    context.lineTo(250, 189);
    context.lineTo(350, 189);

    context.fill();
    context.stroke();


    //hexagon
    const a = 2 * Math.PI / 6;
    const r = 150;
    const x = 800;
    const y = 200;
    context.beginPath();

    //"using for loop"

    // for (var i = 0; i < 6; i++) {
    //     context.lineTo(x + r * Math.cos(a * i), y + r * Math.sin(a * i));
    // }

    //did not using for loop
    context.lineTo(x + r * Math.cos(a * 0), y + r * Math.sin(a * 0));
    context.lineTo(x + r * Math.cos(a * 1), y + r * Math.sin(a * 1));
    context.lineTo(x + r * Math.cos(a * 2), y + r * Math.sin(a * 2));
    context.lineTo(x + r * Math.cos(a * 3), y + r * Math.sin(a * 3));
    context.lineTo(x + r * Math.cos(a * 4), y + r * Math.sin(a * 4));
    context.lineTo(x + r * Math.cos(a * 5), y + r * Math.sin(a * 5));
    context.closePath();
    context.stroke();


    //text
    context.fillStyle="black";
    context.font = "30px Helvetica ";
    context.fillText("Anyone want to join the party?", 220, 400);

    context.fillStyle="black";
    context.font = "40px Times New Roman ";
    context.fillText("Welcome to the canvas ", 380, 50);
    
    context.fillStyle="red";
    context.font = "30px Courier ";
    context.fillText("Hexagon", 740, 200);



}


canvasPage();
